[Educa Saúde](http://www.educasaude.org) é um site brasileiro que tem como foco ajudar as pessoas a encontrar médicos especializados em cada área de atuação e cidade. 
O fazemos da seguinte maneira, primeiro definimos qual a especialidade procurada. 
Logo, estabelecemos qual a cidade objeto da pesquisa. 
Em base a isto, selecionamos aqueles médicos, clínicas ou consultórios que melhor se adaptam as necessidades estabelecidas, tudo para que você não tenha que virar de cabeça para baixo a internet na sua procura de um bom médico.